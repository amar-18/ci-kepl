<!DOCTYPE html>
<html>
<head>
<style>
table {
 font-family: arial, sans-serif;
 border-collapse: collapse;
 width: 100%;
}
td, th {
 border: 1px solid #dddddd;
 text-align: left;
 padding: 8px;
}
tr:nth-child(even) {
 background-color: #dddddd;
}
</style>
</head>
<body>
<h2>Daftar Printer</h2>
<br></br>
<a href="add?>">Tambah Data</a> 
<br></br>
<table>
 <tr>
 <th>Merk Printer</th>
 <th>Warna</th>
 <th>Jumlah</th>
 <th>Action</th>
 </tr>
<?php foreach ($printer as $printer):
?>
<tr>
 <td><?php echo $printer->nama_merk ?></td>
 <td><?php echo $printer->warna ?></td>
 <td><?php echo $printer->jumlah ?></td>
<td><a href="edit/<?php echo $printer->no ?> " >Edit</a> || <a
href="delete/<?php echo $printer->no ?>">Delete</a></td>
 </tr>
 <?php endforeach; ?>
</table>
</body>
</html> 