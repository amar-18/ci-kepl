<?php defined('BASEPATH') OR exit('No direct script access
allowed');

class Printer_model extends CI_Model
{
 // isi sesuai dengan nama tabel kita
 private $_table = "data";
 //isi seusai field tabel kita
 public $no;
 public $nama_merk;
 public $warna;
 public $jumlah;
 // kita buatkan rules untuk input ke tabel printer
 public function rules()
 {
 return [
 ['field' => 'nama_merk',
 'label' => 'Merk Printer',
 'rules' => 'required'],
 ['field' => 'warna',
 'label' => 'Warna Printer',
 'rules' => 'required'],

 ['field' => 'jumlah',
 'label' => 'Jumlah Printer',
 'rules' => 'numeric']
 ];

 }
 public function getAll()
 {
 return $this->db->get($this->_table)->result();
 // perintah di atas kurang lebih sama dengan
 // Select * from printer
 }

 public function getById($id)
 {
 return $this->db->get_where($this->_table, ["no"
=> $id])->row();
 // perintah di atas kurang lebih sama dengan
 // Select * from printer where no - $id
 }
 public function save()
 {
 $post = $this->input->post();
 $this->no = uniqid();
 $this->nama_merk = $post["nama_merk"];
 $this->warna = $post["warna"];
 $this->jumlah = $post["jumlah"];
 return $this->db->insert($this->_table, $this);
 }
 public function update()
 {
 $post = $this->input->post();
 $this->id = $post["no"];
 $this->nama_merk= $post["nama_merk"];
 $this->warna = $post["warna"];
 $this->jumlah = $post["jumlah"];
 return $this->db->update($this->_table, $this,
array('no' => $post['no']));
 }
 public function delete($id)
 {
 return $this->db->delete($this->_table,
array("no" => $id));
 }
}
