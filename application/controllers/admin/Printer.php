<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Printer extends CI_Controller
{
 public function __construct()
 {
 	parent::__construct();
 $this->load->model("Printer_model");
 $this->load->library('form_validation');
 }
 public function index()
 {
 $data["printer"] = $this->Printer_model->getAll();
 $this->load->view("admin/list", $data);
 }
 public function add()
 {
 $printer = $this->Printer_model;
 $validation = $this->form_validation;
 $validation->set_rules($printer->rules());
 if ($validation->run()) {
 $printer->save();
 $this->session->set_flashdata('success', 'Berhasil
disimpan');
header("location: ../printer");
 }
 $this->load->view("admin/new_form");
 }
 public function edit($id = null)
 {
 if (!isset($id)) redirect('admin/printer');

 $printer = $this->Printer_model;
 $validation = $this->form_validation;
 $validation->set_rules($printer->rules());
 if ($validation->run()) {
 $printer->update();
 $this->session->set_flashdata('success', 'Berhasil
disimpan');
header("location: ..");
 }
 $data["printer"] = $printer->getById($id);
 if (!$data["printer"]) show_404();

 $this->load->view("admin/edit_form", $data);
 }
 public function delete($id=null)
 {
if (!isset($id)) show_404();

 if ($this->Printer_model->delete($id)) {
 // redirect(site_url('admin/printer'));
header("location: ..");
 }
 }
}
